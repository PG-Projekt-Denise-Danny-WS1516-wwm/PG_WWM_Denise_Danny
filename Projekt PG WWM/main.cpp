//Wer wird Millionär
//Denise Beinlich-Danny Hendrischke
//Projekt PG

#include <iostream>
#include <stdio.h>
#include "stdafx.h"
#include "colors.h"
#include "jclient.h"

int main(){
    
        //Öffnen von Board of Symbols
    system("open ~/BOS/jserver.jar");
    sleep(3);
    
    loeschen();
    groesse(4, 1);
    text(0, "A");
    text(1, "B");
    text(2, "C");
    text(3, "D");
    
    int zustand = 1;
    int feld;
    
    switch (zustand) {
        case 1:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("1. Frage: 50 €\n");
            printf("Was möchte der Landwirt mit seiner Frau, wenn er sich einen Hoferben wünscht?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) rülpsen\nB) Schluckauf bekommen\nC) aufstoßen\nD) Bäuerchen machen\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 3){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 3){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist D) Bäuerchen machen.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben keinen Gewinn erziehlt!\n\n");
                        return(0);
                    }
                }
            }
            
        case 2:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("2. Frage: 100 €\n");
            printf("Wer 250 Milliliter Wein um 23:45 Uhr konsumiert, trinkt ein ...?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) Halb neun\nB) Kurz nach zehn\nC) Punkt elf\nD) Viertel vor zwölf\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 3){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 3){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist D) Viertel vor zwölf.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben keinen Gewinn erziehlt!\n\n");
                        return(0);
                    }
                }
            }
            
        case 3:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("3. Frage: 200 €\n");
            printf("Im Alphabet kann man zwischen „L“ und „N“ das ...?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) „E“ motion\nB) „A“ nung\nC) „M“ finden\nD) „G“ fühl\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 2){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                        
                    }
                    else if (feld != 2){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist C) „M“ finden.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben keinen Gewinn erziehlt!\n\n");
                        return(0);
                    }
                }
            }
        case 4:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("4. Frage: 300 €\n");
            printf("Welcher „ernährungsbedingte Schönheitsfehler“ lässt sich zum Glück nach dem Motto „wisch und weg“ beheben?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) Milchbart\nB) Kartoffelnase\nC) Wurstfinger\nD) Orangenhaut\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 0){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 0){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist A) Milchbart.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben keinen Gewinn erziehlt!\n\n");
                        return(0);
                    }
                }
            }
        case 5:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("5. Frage: 500 €\n");
            printf("Wer das Wandern grundsätzlich ablehnt, der sagt zu jeder ...?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) Auf Tritt\nB) Gast Spiel\nC) Vor Stellung\nD) Tour Nee\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 3){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 3){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist D) Tour Nee.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben keinen Gewinn erziehlt!\n\n");
                        return(0);
                    }
                }
            }
        case 6:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("6. Frage: 1.000 €\n");
            printf("Was zählt zu den größten Hits von David Bowie?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) Das Supertalent\nB) Undercover Boss\nC) Let´s Dance\nD) Alarm für Cobra 11\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 2){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 2){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist C) Let´s Dance.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 500 € erziehlt, da Sie auf die 500 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }
        case 7:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("7. Frage: 2.000 €\n");
            printf("Was wird im Modegeschäft in der Five-Pocket-Variante angeboten?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) Schuhe\nB) Jeans\nC) Hemden\nD) BHs\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 1){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 1){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist B) Jeans.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 500 € erziehlt, da Sie auf die 500 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }

        case 8:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("8. Frage: 4.000 €\n");
            printf("Wer wurde 1971 im Alter von 23 Jahren der DDR wohl jüngster Rechtsanwalt?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) Gregor Gysi\nB) Joachim Gauck\nC) Achim Mentzel\nD) Gerhard Schröder\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 0){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 0){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist A) Gregor Gysi.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 500 € erziehlt, da Sie auf die 500 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }
        case 9:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("9. Frage: 8.000 €\n");
            printf("Was ist ein „Tweef“?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) USB-Anschlussstelle\nB) T-Shirt mit Knopfleiste\nC) Rindersteak mit Fettrand\nD) Streit via Twitter\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 3){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 3){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist D) Streit via Twitter.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 500 € erziehlt, da Sie auf die 500 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }
        case 10:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("10. Frage: 16.000 €\n");
            printf("Wer oder was war einmal auf der Rückseite des 1000-DM-Scheins abgebildet?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) Limburger Dom\nB) Klingelbeutel\nC) Füllhorn\nD) Franz-Peter Tebartz-van Elst\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 0){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 0){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist A) Limburger Dom.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 500 € erziehlt, da Sie auf die 500 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }
        case 11:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("11. Frage: 32.000 €\n");
            printf("Wie viele Zahlen zwischen 10 und 1.000 haben die Quersumme 2?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) 3\nB) 5\nC) 15\nD) 30\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 1){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 1){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist B) 5.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 16.000 € erziehlt, da Sie auf die 16.000 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }
        case 12:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("12. Frage: 64.000 €\n");
            printf("In der Antarktis gibt es ...?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) eine U-Bahn\nB) zwei Geldautomaten\nC) drei Kindergärten\nD) vier Spaßbäder\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 1){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 1){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist B) zwei Geldautomaten.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 16.000 € erziehlt, da Sie auf die 16.000 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }
        case 13:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("13. Frage: 125.000 €\n");
            printf("Wer oder was erreicht schon mal ein Gewicht von rund einer Tonne?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) Löwe\nB) Formel-1-Wagen\nC) Kürbis\nD) Sumoringer\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 2){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                    }
                    else if (feld != 2){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist C) Kürbis.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 16.000 € erziehlt, da Sie auf die 16.000 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }
        case 14:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("14. Frage: 500.000 €\n");
            printf("Was besteht im Wesentlichen aus „Te Ika a Maui“ und „Te Waka a Maui“?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) Sushi\nB) Ukulele\nC) Neuseeland\nD) Judo-Anzug\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 2){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                        
                    }
                    else if (feld != 2){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist C) Neuseeland.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 16.000 € erziehlt, da Sie auf die 16.000 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }
        case 15:
            statusText("Bitte Frage in der Konsole ansehen und hier beantworten.");
            printf("15. Frage: 1.000.000 €\n");
            printf("Aus insgesamt wie vielen Sternchen besteht der klassische von Ernö Rubik erfundene Zauberwürfel?\n\n");
            printf("Ihre möglichen Antworten:\n");
            printf("A) 20\nB) 22\nC) 24\nD) 26\n");
            
            for ( ; ; ){
                char *a = abfragen();
                if (strlen(a) > 0 && a[0] == '#'){
                    sscanf(a,"# %d",&feld);
                    if (feld == 3){
                        farbe(feld, GREEN);
                        printf("\n\nDie Antwort ist richtig!\n\nHERZLICHEN GLÜCKWUNSCH, SIE HABEN 1.000.000 EURO GEWONNEN\n\n");
                            sleep(2);
                            loeschen();
                            zustand++;
                            break;
                        
                    }
                    else if (feld != 3){
                        farbe(feld, RED);
                        printf("\n\nDie Antwort ist leider falsch! Die richtige Antwort ist D) 26.\n");
                        printf("Das Spiel ist daher leider vorbei und Sie haben einen Gewinn von 16.000 € erziehlt, da Sie auf die 16.000 € Frage zurückfallen!\n\n");
                        return(0);
                    }
                }
            }

            
                
    }
    
}
