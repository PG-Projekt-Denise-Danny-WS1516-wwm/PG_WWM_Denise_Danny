Unser Projekt heißt PG_WWM_Denise_Danny.
Wir programmieren das Spiel “Wer wird Millionär“. Es soll dem originalen Spiel, welches aus dem Fernsehen bekannt ist, ähneln.
Um die eine Millionen zu Gewinnen, muss der Spieler 15 Fragen richtig beantworten. Bei den Fragen handelt es sich um original Fragen, die in der Fernsehsendung gestellt wurden.
Die Fragen werden nach jeder Runde schwieriger, außerdem wird es zu jeder der 15 Level eine Fragen geben. Zusätzlich versuchen wir die Umsetzung eines Jokers, was sich allerdings bisher als schwierige Aufgabe herausgestellt hat. 
Um unsere Spielidee umsetzen zu können, arbeiten wir mit printf, case- Anweisungen, do/while Anweisungen, sowie mit if/else Anweisungen.